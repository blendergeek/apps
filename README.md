# Soft Forked F-Droid Client

[![build status](https://gitlab.com/fdroid/fdroidclient/badges/master/build.svg)](https://gitlab.com/fdroid/fdroidclient/builds)
[![Translation status](https://hosted.weblate.org/widgets/f-droid/-/svg-badge.svg)](https://hosted.weblate.org/engage/f-droid/)

Fork of the official client for [F-Droid](https://f-droid.org), the Free Software repository system
for Android. This fork has a modified user interface that attempts to solve some issues that users have had with the new UI.

## Building with Gradle

    ./gradlew assembleRelease

## Direct download

I currently do not have pre-built apk's for this app. I hope to include it in [F-Droid](https://f-droid.org/) soon.

## Contributing

See our [Contributing doc](CONTRIBUTING.md) for information on how to report
issues, translate the app into your language or help with development.

## IRC

F-Droid developers are on `#fdroid` and `#fdroid-dev` on Freenode.

## FAQ

* Why a fork of F-Droid?

This fork of F-Droid intends to address issues users have with the new 
interface. This is a soft fork that intends to track continued development
and all contributions *should* be offered upstream first.

* Why does F-Droid require "Unknown Sources" to install apps by default?

Because a regular Android app cannot act as a package manager on its
own. To do so, it would require system privileges (see below), similar
to what Google Play does.

* Can I avoid enabling "Unknown Sources" by installing F-Droid as a
  privileged system app?

This used to be the case, but no longer is. Now the [Privileged
Extension](https://gitlab.com/fdroid/privileged-extension) is the one that should be placed in
the system. It can be bundled with a ROM or installed via a zip.
Please note that as I am a newb at all this I have not gotten a prvileged
extension working with this fork yet.

## License

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Some icons are made by [Picol](http://www.flaticon.com/authors/picol),
[Icomoon](http://www.flaticon.com/authors/icomoon) or
[Dave Gandy](http://www.flaticon.com/authors/dave-gandy) from
[Flaticon](http://www.flaticon.com) or by Google and are licensed by
[Creative Commons BY 3.0](https://creativecommons.org/licenses/by/3.0/).

Other icons are from the
[Material Design Icon set](https://github.com/google/material-design-icons)
released under an
[Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).


## Translation

Everything can be translated.  See
[Translation and Localization](https://f-droid.org/docs/Translation_and_Localization)
for more info.
[![translation status](https://hosted.weblate.org/widgets/f-droid/-/f-droid/multi-auto.svg)](https://hosted.weblate.org/engage/f-droid/?utm_source=widget)

Some strings related to the fork have not been translated. Help would be
appreciated as I only know English. Until then, the rest of the app will
be translated.
